* XPFE-Classic SeaMonkey Theme
  :PROPERTIES:
  :CUSTOM_ID: gray-modern-revived-seamonkey-theme
  :END:

#+CAPTION: Preview
[[./screenshots/seamonkey.png]]

** About
   :PROPERTIES:
   :CUSTOM_ID: about
   :END:

  + Emulates classic Mozilla appearance
  + Themes has been updated to work with SeaMonkey version 2.49.1
  + Restore original Mozilla throbber

** Install
   :PROPERTIES:
   :CUSTOM_ID: install
   :END:

=git clone https://gitlab.com/metsatron/xpfeclassic-mozilla.org.git ~/.mozilla/seamonkey/**.default/extensions/xpfeclassic@mozilla.org/=

** Screenshots
[[./screenshots/mail.png]]
[[./screenshots/lightning.png]]
[[./screenshots/address.png]]
[[./screenshots/composer.png]]
